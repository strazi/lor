function toggleElement(element) {
  if (element.classList.contains('is-visible')) {
    element.classList.add('is-removing');
  } else {
    element.classList.add('is-visible');
  }

  element.addEventListener('animationend', function() {
    if (element.classList.contains('is-removing')) {
      element.classList.remove('is-removing', 'is-visible');
    }
  });
}

var headerMenuButton = document.querySelector('.header__menu');
var headerMenuContent = document.querySelector('.header__menu-content');
var profileMenuButton = document.querySelector('.header__profile');
var profileMenuContent = document.querySelector('.header__profile-content');
var expandableHeader = document.querySelector('.expandable__header');
var outlineHasChildren = document.querySelectorAll('.has-children');
var browserGroupTitle = document.querySelectorAll('.browser__group-title');

headerMenuButton.addEventListener('click', function() {
  headerMenuButton.classList.toggle('is-active');
  toggleElement(headerMenuContent);
});

profileMenuButton.addEventListener('click', function() {
  profileMenuButton.classList.toggle('is-active');
  toggleElement(profileMenuContent);
});

if (expandableHeader) {
  expandableHeader.addEventListener('click', function() {
    expandableHeader.parentNode.classList.toggle('is-active');
  });
}

if (outlineHasChildren) {
  for (i = 0; i < outlineHasChildren.length; i++) {
    outlineHasChildren[i].addEventListener('click', function(e) {
      e.target.classList.toggle('is-open');
      e.stopPropagation();
    });
  }
}

if (browserGroupTitle) {
  for (i = 0; i < browserGroupTitle.length; i++) {
    browserGroupTitle[i].addEventListener('click', function(e) {
      e.target.parentNode.classList.toggle('is-open');
      e.stopPropagation();
    });
  }
}

