var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var ssi = require('browsersync-ssi');

var src = {
  sass: './sass/**/*.scss',
  css:  './app/css',
  html: './app/**/*.html'
};

gulp.task('serve', ['sass'], function() {
  browserSync.init({
    server: './app',
    notify: false,
    ghostMode: false,
    middleware: ssi({
      baseDir:  __dirname + '/app',
      ext: '.html'
    })
  });

  gulp.watch(src.sass, ['sass']);
  gulp.watch(src.html).on('change', reload);
});

gulp.task('sass', function() {
  return gulp.src(src.sass)
    .pipe(sourcemaps.init())
    .pipe(
      sass(
        {
          includePaths: ['./sass'],
          outputStyle: 'compressed',
          errLogToConsole: true,
          sourceMap: true,
          outFile: src.css
        }
      ).on('error', sass.logError)
    )
    .pipe(autoprefixer())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(src.css))
    .pipe(browserSync.stream({match: '**/*.css'}));
});

gulp.task('default', ['serve']);